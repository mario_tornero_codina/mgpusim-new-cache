// Code generated by "esc -private -pkg=layers -o=bindata.go trans.hsaco gpu_gemm.hsaco relu.hsaco"; DO NOT EDIT.

package layers

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"sync"
	"time"
)

type _escLocalFS struct{}

var _escLocal _escLocalFS

type _escStaticFS struct{}

var _escStatic _escStaticFS

type _escDirectory struct {
	fs   http.FileSystem
	name string
}

type _escFile struct {
	compressed string
	size       int64
	modtime    int64
	local      string
	isDir      bool

	once sync.Once
	data []byte
	name string
}

func (_escLocalFS) Open(name string) (http.File, error) {
	f, present := _escData[path.Clean(name)]
	if !present {
		return nil, os.ErrNotExist
	}
	return os.Open(f.local)
}

func (_escStaticFS) prepare(name string) (*_escFile, error) {
	f, present := _escData[path.Clean(name)]
	if !present {
		return nil, os.ErrNotExist
	}
	var err error
	f.once.Do(func() {
		f.name = path.Base(name)
		if f.size == 0 {
			return
		}
		var gr *gzip.Reader
		b64 := base64.NewDecoder(base64.StdEncoding, bytes.NewBufferString(f.compressed))
		gr, err = gzip.NewReader(b64)
		if err != nil {
			return
		}
		f.data, err = ioutil.ReadAll(gr)
	})
	if err != nil {
		return nil, err
	}
	return f, nil
}

func (fs _escStaticFS) Open(name string) (http.File, error) {
	f, err := fs.prepare(name)
	if err != nil {
		return nil, err
	}
	return f.File()
}

func (dir _escDirectory) Open(name string) (http.File, error) {
	return dir.fs.Open(dir.name + name)
}

func (f *_escFile) File() (http.File, error) {
	type httpFile struct {
		*bytes.Reader
		*_escFile
	}
	return &httpFile{
		Reader:   bytes.NewReader(f.data),
		_escFile: f,
	}, nil
}

func (f *_escFile) Close() error {
	return nil
}

func (f *_escFile) Readdir(count int) ([]os.FileInfo, error) {
	if !f.isDir {
		return nil, fmt.Errorf(" escFile.Readdir: '%s' is not directory", f.name)
	}

	fis, ok := _escDirs[f.local]
	if !ok {
		return nil, fmt.Errorf(" escFile.Readdir: '%s' is directory, but we have no info about content of this dir, local=%s", f.name, f.local)
	}
	limit := count
	if count <= 0 || limit > len(fis) {
		limit = len(fis)
	}

	if len(fis) == 0 && count > 0 {
		return nil, io.EOF
	}

	return fis[0:limit], nil
}

func (f *_escFile) Stat() (os.FileInfo, error) {
	return f, nil
}

func (f *_escFile) Name() string {
	return f.name
}

func (f *_escFile) Size() int64 {
	return f.size
}

func (f *_escFile) Mode() os.FileMode {
	return 0
}

func (f *_escFile) ModTime() time.Time {
	return time.Unix(f.modtime, 0)
}

func (f *_escFile) IsDir() bool {
	return f.isDir
}

func (f *_escFile) Sys() interface{} {
	return f
}

// _escFS returns a http.Filesystem for the embedded assets. If useLocal is true,
// the filesystem's contents are instead used.
func _escFS(useLocal bool) http.FileSystem {
	if useLocal {
		return _escLocal
	}
	return _escStatic
}

// _escDir returns a http.Filesystem for the embedded assets on a given prefix dir.
// If useLocal is true, the filesystem's contents are instead used.
func _escDir(useLocal bool, name string) http.FileSystem {
	if useLocal {
		return _escDirectory{fs: _escLocal, name: name}
	}
	return _escDirectory{fs: _escStatic, name: name}
}

// _escFSByte returns the named file from the embedded assets. If useLocal is
// true, the filesystem's contents are instead used.
func _escFSByte(useLocal bool, name string) ([]byte, error) {
	if useLocal {
		f, err := _escLocal.Open(name)
		if err != nil {
			return nil, err
		}
		b, err := ioutil.ReadAll(f)
		_ = f.Close()
		return b, err
	}
	f, err := _escStatic.prepare(name)
	if err != nil {
		return nil, err
	}
	return f.data, nil
}

// _escFSMustByte is the same as _escFSByte, but panics if name is not present.
func _escFSMustByte(useLocal bool, name string) []byte {
	b, err := _escFSByte(useLocal, name)
	if err != nil {
		panic(err)
	}
	return b
}

// _escFSString is the string version of _escFSByte.
func _escFSString(useLocal bool, name string) (string, error) {
	b, err := _escFSByte(useLocal, name)
	return string(b), err
}

// _escFSMustString is the string version of _escFSMustByte.
func _escFSMustString(useLocal bool, name string) string {
	return string(_escFSMustByte(useLocal, name))
}

var _escData = map[string]*_escFile{

	"/gpu_gemm.hsaco": {
		name:    "gpu_gemm.hsaco",
		local:   "gpu_gemm.hsaco",
		size:    13800,
		modtime: 1578872529,
		compressed: `
H4sIAAAAAAAC/+xby28TVxc/cz0eO5MH+b5uCKB2mlYKIGZij+PEyYa8moBIQiAtj1YU3XiuHSfzsMbj
NKnAhLRELJCKqkpd0mWl9m8gQeqiy4Y1i26QUFds2mVdzePaM04MQTwF85M8Z+ace+45v3vnjn3H9177
ZGoCMcwweIjAn8DYJ4J7TQ2PZFcedXQZiMMwtAEPHACwvnKNcosJyrinZzy/ZviyMyihs+4X9eXXKK+g
oPT72blCwtM3yCIEJfVDz+hH+Z19aCnsHvz8+dk489BSOHh2sLQ9Ke8G+bg9KFmfX9yLPzI97hSnfXPQ
uR9cPQuxGjeqG5ken5z9zC3bBQCtnh5rSj6ri1hT7M9CCYtiPreSSaS8evV2AN4rK4oif46YpYKhDwkU
XwjJY0JCuMSfIqZO1NIQLwiiMIM1Ui8jCEKeaBpvn8ytavOG6rP32KbhJaXHMU9hPV/G+brz6SLRx6aE
sYC1loUTXRYuOdYRM++Et7FLChpPzz5dLZKAuaBbNeNc4eugX1/NNKIW8vrQrqZzWC2TUwVdoebRVUcV
LGAHpgVOpuR6xdnsmTJW61WPkxwuq1ZzMvrbRGbpbSKD1eICbk4opxr41VOaeC5K88R6yxiNNKfT4/A5
2tOcUaY5o0xzRpOqMY/V0XIuR8w90lIUc66Is4SSc6t4Dtqj7ybtsXeT9vg7QfsFZX6ioChEd4OfzuVK
xLrwhG+I/r6XH//ia47/+WuIP2PoT/pizuzxtgmzem1ZTZdVqzBpFpS5VT07YuafO8MxQyGzplGs/bS3
JxrYzM+RvEZ0y00+maRPlEnTKBc920RhhShugYRnnjULy9gizQsEa/f400zP42WSMw0aVRBq42CmrM1N
zp4t1doqla5bzgUsSRpqGq9MqNg6b5hLbtZOpXK6n5ckiW8+f7TnfAci3I55NOP7HHBVa/ZE0L7+ZeJH
pxiC4BzWgT0DgxAhQoQIESJEiBAhQoQIESLEmwDGm78zzr+7kYZJ/E4g5le43QLQCsGXCUXf+UcNNpZl
uWq1Wn0T+fNw++46QluszT7Cb4HTCvxWxvlr/qe71+DWJl9lNuzs40z8e4hHrrMbsBZdY9YZxFUAsRWA
Ew8QAIoAyAC//dEOCIpwaxPBjfstLALEsjKKILkN3bwKi2sbIKzfPQ43N78BtNVpx2M6tvYDQMd1dnMN
sZWP4Ybj24EQ2PGjiK9ApK1yh+W6I/Dd/XUWgV2O5Tg52hLPdCC+cqetoztm29oQcNC13d6GIIb+X4lB
1zbXgSAOh7Zb/mf39OyDGEDMlhGA+D2el9f4H65GoGv7Ww5BZfGvDRYOba/FXQ7xfeyxR9WNTQZu3GcO
gpNPC4pXWhFfYRiQ7wDqBjsu2DE4mY3EM17dER5AjkZQxmkLjuP3sdwxgOIDZwEBXN8MR1+IECFChAgR
IkSIECFCvDrQteaP97my1bve78moJy900vly0O/vf6uGLQXPTteVD3fuHu/E2JiQVbGeF5bd9dZCMiEl
pIRwuFfBFu5dJPpSQS+JXxnmUqmIs6Q3a2jFskVE08hqoklUMSUlesmKRUwdq70L2axoGVavqi5rYtE0
FknW6nUD9CUG0v2p/r6B5CDJpLCsKEoaz5MEzuXIQCIxIKeyipJJy0eEw/O4RBTB0AU7vZSUkJKDfYMp
cSBN8GBaFr2ahCMwVdCXiDkkTE2Nv4zEVVXZe9pPfa9j99KVD4P6mKf/vUHf7ukPdwf173n6lQb9+87L
oFh9X4OHribrSEDSDYuApKzqpVUNpLxelhZwaQG8o623TJAssmI5V1grZEHKGppGdAuk0qpm4XmQSgsl
y3TPXAmjo4nLSeeYco59zjHtLkC5PH5xZmT65NgLeU8W8611aba/ovbOC3a2e6vPjY43KhO+8cb49pHQ
cWgX+6daNag/HW812ZBWHHb2S9Rnp+OTyv0N/myD/MDb94EangdUcrvef3X0+PfgQPN9O80qED3fCFU0
2U8TbeBPH0P9XpUNtysUPcU0s3t4Ko/7+96HraQrf4b685Pbpf8m/bn7sODtq7r4lPY708T/nlxv3yf5
/xcAAP//aVmkveg1AAA=
`,
	},

	"/relu.hsaco": {
		name:    "relu.hsaco",
		local:   "relu.hsaco",
		size:    13904,
		modtime: 1579034408,
		compressed: `
H4sIAAAAAAAC/+xbzW/U1hY//shkMuG9x3ugJ8hb4AfvKYCenRlPPiZZvJKPJkEkIZDykVYI3dh3JpN4
7JHHE5KKDmkWBVWRSkvXbVddsehfkETqP1ChLll0w76q1C6Zyva94w9iAiWUUu5PGh/7fNx7ju+x5/he
+9bbU+M8x50BAgF+AM7dkfxjKpjN+/S0xytAGs7AAchACgDEkF6c7nBRmiZ8jtgl4evDUQoHA7u2kH9x
+r0QpWE711fIEn6MViFKqR3/nHY0vouPHF18Bruwfy4uPHL0FDw/RHo+eQgcD9N/RKkYskuT/oenxzx1
Ojb/8vLB54vQ3oqN8oanxyZmL/m6RwGgk/BRRS9ppowquvtbrCFZLhVXC9k8affW3wEyRFeW5cxlbNfK
ljkkUbwn5f4nZaVrmXPYNrFRG8pIkizNoAoOdCRJuoinLo1b9g1k6xn3eG6tsmAZIbXukMaZZb3b05pC
ZqmOSkFT56vYHJ2SRiPSlk+eL6p0zZMO2yXPGRe7OKRZddPJ0KN31qo4olIOCefK70dte1uiYaNcMod2
FV1GRh2fK5s6FY+seayogtsxVTibV4OGNe1CHRlB02O4iOqGkxxQ2UyOprtoWMg53Z0cUiE5pEJySBOG
tYCMkXqxiO3kuMbDcem6PVdFGqbR+U28QNxW3XkjAt8nzyfLuo5Nv/PzxWINO1efkpH9vS+///lX3P+7
r6D/Gct82o2g8Ixpw7x6ZV5N1w2nPGGX9bk1Uxu2Sy/s4ail41nbqrb+tNw/VGSX5nCpgk3Hd76QJcIJ
26pXiWi8vIp1X07Fs3Z5BTk4WSHaOAmfOnoFreCibdFOJal1GczUK3MTsxdrrVOV7wsklyMSKphGq+MG
cq5Y9rLvtNem2tefXCiMIG15j0qBqrBS4Tf8ZRb9Muv6m1oysFKJlUqsVGKlEvPqz1wqFV6bUql/z1JJ
UZRM4nwSBwBdQgrgOJlPIxNUf6V8Mt/2ORfo01+Xx/Hmkg66xzfvnb59T9+eF8jcEcSmplrzX+EiDEJz
N8DAwMDAwMDAwMDAsB/gSB3Oeau7QrAQnaTP3Ye73lpv9NlhNrR/wl+hD9amRTHVbDabf8T4N4DfEQHg
W+B3/LXt1M4RAOiAj7cy8NXWLdjc5pvcR673aS79WRpABeBVgb/zwZK0vnUI7mwLkNlxz946Dw2ATx98
ATy4dhyfagCIqiCkCgCzD3kAXuDTDR5AFQW+UIXN7Z9EMbMuiv8EqD70H4g+3AZY/91++zX+VyPjn3qt
x1/yxn9z60Di+IMqgD/+J9zx5+n4iw0QUg3BzQGRb+WS20YHn2mkUym1rSPt5YIAIHwJ/HE3B9ZTGzeX
pI2tdrizzcMnDzaAh04+3ciIoiq2B7nT4B/fBiL3cwtUgeRRNH8YGBgYGBgYGBgYGBieROtJjbxn30kO
jxBKn+Q3iZw+9XUR+vPjpuXSSSKn75Ubh3fvb3J0VNIMZJakFf+1KSmXVbJKVjrZoyMH9Sxhc7ls1uQb
lr1cqyIN92hWpVp3sGxbWkW2sSHnlWwPXnWwbSKjZ1HTZMdyegxjpSJXbWsJa06P30FvdqCvP9/fO5Ab
xIU8UnVd70MLOIuKRTyQzQ6oeU3XC33qKenkAqphXbJMyXUvr2SV3GDvYF4e6MNosE+VSUvSKZgqm8vY
HpKmpsZehuOGoT+723vO67ijO/bvKL+d8O/G+H+h+iei/ENUP8Y/Svj3Y/xjhP9djP9/d8O3B99BEPw3
YZ1ZTVhnBsW0HAyKvmbW1iqglMy6sohqi0C2Lt+xQXHwquMdoUpZA0WzKhVsOqDU1ioOWgCltlhzbH/P
pzAykr2e87YqjIzkvP0c2c97297kBerrY/Mzw9NnR/dlPq49vDae9B0HRK+38Ph2hu0PR+lk6LrmQt+r
0Ov9bwDwS7NpUXt6XVP6n5g7aXgyL9pCcnofoFSK2Ysxeoy8I8DH7juUdu2a5wG6w9/6QPL3QUkNyMS2
pZbw3U5bLH7yGQ/0kyZjaQ5Vwpjmdu+e0rdi70VQ7OR8+iME9+n0LuM3EXvHguIb1afze5y/Cwn2Hfmg
/afZ/xoAAP//bciEz1A2AAA=
`,
	},

	"/trans.hsaco": {
		name:    "trans.hsaco",
		local:   "trans.hsaco",
		size:    9656,
		modtime: 1578852356,
		compressed: `
H4sIAAAAAAAC/+xaTXPbxBt/JCuO4+T/B0605YAIh7QdJCtynDi50Lw0SadJmjbQUphOZyOtbTV6G2kd
YgbSF2inM3SGl+FePgCHHjm1OfABmJ576IHO8AXgiBlJu7bkWn2BdgpUv5n4sZ6XfX7PWqvsrvbi0ZVF
nuOOAEUO7gMXfHk1umaGH0cjeTjUVaEAR2AEipAHACHm1yv3uKQsUD1H49Lw9khSMj5B3EDsulf+yiVl
PC7gCiLV90gXkpLF8U8Zx+o79YDowhPExfkFOPmA6Hl4egisP3noEo/JW0NJKcTiCjT/7OpC6M5+mzfC
+yHSCzDYqY3pZlcXltbfj3z3A8Aw1SNLr2u2hCw9+Gv4SJLqtZ2qUqbtykMAReorSVLxNPZ8w7FnRIaP
xPF3REU8VzyOPRub/kxRFCVxDVm46yOK4nsesn3X8XExuNpoWZuOGXMa69iPbOljoc8KsutNVO82c8LF
9vyKOJ+wdviEPFTxXGid9eohkQB9yByzix1eLRcn7GM100Hk8FjHY8P4JBld7ZhmTaNuz/Q1nUZmEx83
bJ2Zl0xnE5lzzVoNe0mvgALzWiyr3dZ13dtwkYZPNpE502mia9c0ZomwgGuoaZL0uk80yctZuNMkbpOc
MXTSSO8AwybptU+k1z6RXvtcK1Sll30sXvZfLGsZG/UG+RfX9YxutGVD17Ed3SsnajUfkw8eQXBy4vnn
P/uC83/4AvKvOfaj7ovqE47yjNULY7XaNImx5Bn6RsvWZr3632Y47+h43XPczr/kYKqAvPoGrlvYJhH5
qkKNS57TdKlp0djBemRn5nXP2EYEpzskG6flM6Jn0DaueQ5LKoqdYbDWtDaW1k/5na4qV7qW0wkLM6yi
nUUTkTOOtxWRDttUK5NFWZaL6fO/YM52IJd/aB7Mxf4O0OmmSK+PTg9/x8emily8wc7kCTJkyPAfBUfH
Pxeu7nI9D4E+/twP8PVQsNZLPozW42v3aIXeXZsKQr7dbrf/ifXzwO8Fa9LPIb8XPBtzueLePgjWp1/e
Hobvb1+EG3egDdcC9gUofDtSKFwWrsGlgUvcFY7P7wIv7AIs3+MB+AKACvDTzzzw4MKNOzxcvTsk8PB/
QVD5HK/m+OufwYVL10D84vYBuB7aYRAgxxX3wr4dENSbQn6Uh6/uXhF4CHIP8IVdQRBUYTBfBVgP83Bw
9S43ADDI53c5DtSbwI9CEAM85AHUXI6vBvkB3HvRw/3ynexOz5AhQ4YMGTJkyJAhQ4aXG+xd863hSFIB
+6hkK/l99D08W/Uzv9/+aDuBvE8VnXf6I/3zLc/Pi5qJ7Lq4Hb1lFccVWZEV8WBJRwSVLmB7y7B96WPH
2/JdpOGS5lhuk2DJczRL8rAplWWlhHcI9mxklhqaJhGHlExz25Jcz7mANVKKEkwoU5XJ8uTE1Pg0rpaR
qut6BW1iBdVqeEpRptSypuvVinpIPLiJfKyLji0G9MqyIo9PT0yXpakKRtMVVaItiYdgxbC3sDcjrqws
PA/ipqk/Oe3H7usEv+6nryX1g1T/S4/+9XAzZLB7ToHifyn7yiDbDsEg6y3bb1kg1+2m3EB+A+hnoCce
yATvkPAKWYYGsuZYFrYJyH7LImgTZL/hEy/6FkmYm1POj4efamwT+vzC2bXZ1WPzz3LfazC29512XqKz
hwUP9+NwLIyNHyaV2PjhYudC2Lh6BQB+b7cdFs/GD5NiD61CT/79tG2+d7yNJPPwPfyZfJOe4+B7xjeT
w33vpy7G4mdqIP0cTloDEo3NMUXK+ZiBnvpZmknapNKTxqXxq1z/9Ey+G++7GPbeiuRO7Dkn9Pn9luLc
Y6jSc1JnH9N/J1PivxlNdkda/J8BAAD//5VIJrK4JQAA
`,
	},
}

var _escDirs = map[string][]os.FileInfo{}
